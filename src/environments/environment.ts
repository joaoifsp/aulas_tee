// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBXk2ON3ql-zR8PH6qLezacekvD9WFAcAo',
    authDomain: 'controle-jv.firebaseapp.com',
    projectId: 'controle-jv',
    storageBucket: 'controle-jv.appspot.com',
    messagingSenderId: '745020897882',
    appId: '1:745020897882:web:0b1e095c82b27d211bb663',
    measurementId: 'G-HG5SS034YC'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
